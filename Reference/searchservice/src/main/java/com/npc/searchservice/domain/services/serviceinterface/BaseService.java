package com.npc.searchservice.domain.services.serviceinterface;

public interface BaseService<T> {

    T getById(String id);

    void deleteById(String id);

    void save(T object);

    void update(T object);
}
