package com.npc.searchservice.domain.services;

import com.npc.searchservice.domain.entities.Company;
import com.npc.searchservice.domain.services.serviceinterface.CompanyService;
import com.npc.searchservice.infrastructure.repositories.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyServiceImp implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public Page<Company> findAllByUserId(String userId, int page, int size,
                                         String sort, String sortBy) {

        return null;
    }

    @Override
    public Company getById(String id) {
        return companyRepository.getById(id);
    }

    @Override
    public void deleteById(String id) {

    }

    @Override
    public void save(Company company) {
        companyRepository.save(company);
    }

    @Override
    public void update(Company company) {

    }

    @Override
    public List<Company> findAll() {
        return companyRepository.findAll();
    }

    @Override
    public void generateData() {
        Company company = new Company();
        company.setCode("a");
        company.setName("a");
        company.setUserId("a");

        this.save(company);
    }
}
