package com.npc.searchservice.infrastructure.repositories;

import com.npc.searchservice.domain.entities.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface CompanyRepository extends ElasticsearchRepository<Company, String> {

    Page<Company> findAllByUserId(String userId, Pageable pageable);

    Company getById(String companyId);

    List<Company> findAll();
}
