package com.npc.searchservice.domain.services.serviceinterface;

import com.npc.searchservice.domain.entities.Company;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CompanyService extends BaseService<Company> {
    Page<Company> findAllByUserId(String userId, int page, int size,
                                  String sort, String sortBy);

    List<Company> findAll();

    void generateData();
}
