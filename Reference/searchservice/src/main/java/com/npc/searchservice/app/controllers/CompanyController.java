package com.npc.searchservice.app.controllers;

import com.npc.searchservice.domain.entities.Company;
import com.npc.searchservice.domain.services.serviceinterface.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @GetMapping("/hello-world")
    public ResponseEntity<?> helloWorld() {
        return ResponseEntity.ok("Hello World!");
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") String id) {
        return ResponseEntity.ok(companyService.getById(id));
    }

    @GetMapping
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(companyService.findAll());
    }

    @PostMapping
    public ResponseEntity<?> generateData() {
        companyService.generateData();
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
