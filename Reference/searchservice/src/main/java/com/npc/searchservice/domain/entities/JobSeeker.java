package com.npc.searchservice.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "references", type = "job-seekers")
public class JobSeeker {
    @Id
    private String id;
    private String title;
    private String code;
    private String name;
}
