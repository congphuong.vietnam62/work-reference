package com.companyservice.infrastructure.repositories;

import com.companyservice.domain.entities.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepositories extends JpaRepository<Company, String> {

    List<Company> findAll();


}
