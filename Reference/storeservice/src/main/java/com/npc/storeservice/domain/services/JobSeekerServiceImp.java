package com.npc.storeservice.domain.services;

import com.npc.storeservice.domain.services.serviceinterface.JobSeekerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

import static org.apache.http.entity.ContentType.*;

@Service
public class JobSeekerServiceImp implements JobSeekerService {

    @Value("${aws.s3.bucketName}")
    private String bucketName;

    @Autowired
    private StorageService storageService;

    @Override
    public void saveFile(MultipartFile multipartFile) {
        if (multipartFile.isEmpty()) throw new IllegalStateException("Cannot upload empty file");

        if (!Arrays.asList(IMAGE_PNG.getMimeType(), IMAGE_BMP.getMimeType(), IMAGE_GIF.getMimeType(),
                IMAGE_JPEG.getMimeType()).contains(multipartFile.getContentType())) {
            throw new IllegalStateException("FIle uploaded is not an image");
        }

        Map<String, String> metadata = new HashMap<>();

        metadata.put("Content-Type", multipartFile.getContentType());
        metadata.put("Content-Length", String.valueOf(multipartFile.getSize()));

        String path = String.format("%s/%s", bucketName, UUID.randomUUID());
        String filename = String.format("%s", multipartFile.getOriginalFilename());
        try {
            storageService.uploadFile(path, filename, Optional.of(metadata), multipartFile.getInputStream());
        } catch (Exception e) {
            throw new IllegalStateException("Failed to upload file", e);
        }
    }

    @Override
    public byte[] downloadFile(String path, String fileName) throws IllegalAccessException {
        return storageService.downloadFile(path, fileName);
    }
}
