package com.npc.storeservice.domain.services;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import com.npc.storeservice.app.configs.AmazonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.Map;
import java.util.Optional;

@Service
public class StorageService {

    @Autowired
    private AmazonClient amazonS3;

    public void uploadFile(String path, String fileName, Optional<Map<String, String>> optionalMetaData,
                           InputStream inputStream) {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        optionalMetaData.ifPresent(map -> {
            if (!map.isEmpty()) {
                map.forEach(objectMetadata::addUserMetadata);
            }
        });
        try {
            amazonS3.awsClient().putObject(path, fileName, inputStream, objectMetadata);
        } catch (AmazonServiceException e) {
            throw new IllegalStateException("Failed to upload the file", e);
        }
    }

    public byte[] downloadFile(String path, String key) throws IllegalAccessException {
        try {
            S3Object s3Object = amazonS3.awsClient().getObject(path, key);
            S3ObjectInputStream objectInputStream = s3Object.getObjectContent();

            return IOUtils.toByteArray(objectInputStream);
        } catch (Exception e) {
            throw new IllegalAccessException("Failed to download file with path :" + path + " and key: " + key);
        }
    }
}
