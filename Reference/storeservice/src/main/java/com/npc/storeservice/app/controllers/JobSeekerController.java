package com.npc.storeservice.app.controllers;

import com.npc.storeservice.domain.services.serviceinterface.JobSeekerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/job_seekers")
public class JobSeekerController {

    @Autowired
    private JobSeekerService jobSeekerService;

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile multipartFile) {
        jobSeekerService.saveFile(multipartFile);
        return ResponseEntity.ok("OKe");
    }

    @GetMapping
    public ResponseEntity<?> downloadFile(@RequestParam("filename") String filename,
                                          @RequestParam("path") String path) throws IllegalAccessException {
        return ResponseEntity.ok().contentType(MediaType.valueOf("image/jpg"))
                .body(jobSeekerService.downloadFile(path, filename));
    }
}
