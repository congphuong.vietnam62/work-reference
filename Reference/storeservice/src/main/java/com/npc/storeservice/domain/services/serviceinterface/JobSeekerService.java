package com.npc.storeservice.domain.services.serviceinterface;


import org.springframework.web.multipart.MultipartFile;

public interface JobSeekerService {
    void saveFile(MultipartFile multipartFile);

    byte[] downloadFile(String path, String fileName) throws IllegalAccessException;
}
