package com.npc.syncservice.infrastructure.repositories.postgres;

import com.npc.syncservice.domain.entities.postgres.CompanyPostgres;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface CompanyPostgresRepository extends JpaRepository<CompanyPostgres, String> {

    CompanyPostgres getById(String id);

    List<CompanyPostgres> findAll();
}
