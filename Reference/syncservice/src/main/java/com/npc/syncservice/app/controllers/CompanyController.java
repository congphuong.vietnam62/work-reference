package com.npc.syncservice.app.controllers;

import com.npc.syncservice.domain.services.serviceinterface.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @GetMapping("/hello-redis")
    public ResponseEntity<?> helloRedis() {

        companyService.testRedis();
        return ResponseEntity.ok("OKe Redis");
    }

    @GetMapping("/hello-postgres")
    public ResponseEntity<?> helloPostgres() {
        return ResponseEntity.ok(companyService.testPostgres());
    }

    @GetMapping("/hello-mongodb")
    public ResponseEntity<?> helloMongoDB() {
        return ResponseEntity.ok(companyService.testMongoDb());
    }

    @GetMapping("/hello-elasticsearch")
    public ResponseEntity<?> helloElasticSearch() {
        return ResponseEntity.ok(companyService.testElasticSearch());
    }

    @GetMapping("/hello-kafka")
    public ResponseEntity<?> helloKafka(@RequestParam("message") String message) {

        companyService.publishToTopic(message);
        return ResponseEntity.ok("OKe kafka");
    }
}
