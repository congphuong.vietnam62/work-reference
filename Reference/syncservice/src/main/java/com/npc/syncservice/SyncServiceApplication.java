package com.npc.syncservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableElasticsearchRepositories(basePackages = "com.npc.syncservice.infrastructure.repositories.elasticsearch")
@EnableMongoRepositories(basePackages = "com.npc.syncservice.infrastructure.repositories.mongodb")
@SpringBootApplication
public class SyncServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SyncServiceApplication.class, args);
	}

}
