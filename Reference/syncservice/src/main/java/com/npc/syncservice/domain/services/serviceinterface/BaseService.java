package com.npc.syncservice.domain.services.serviceinterface;

import java.util.List;

public interface BaseService<T> {

    T getById(String id);

    void deleteById(String id);

    void save(T object);

    void update(T object);

    List<T> findAll();
}

