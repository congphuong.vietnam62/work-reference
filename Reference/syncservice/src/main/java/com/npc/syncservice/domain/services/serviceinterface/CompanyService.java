package com.npc.syncservice.domain.services.serviceinterface;

import com.npc.syncservice.domain.entities.elasticsearch.CompanyES;
import com.npc.syncservice.domain.entities.mongodb.CompanyMongodb;
import com.npc.syncservice.domain.entities.postgres.CompanyPostgres;

public interface CompanyService extends BaseService<CompanyPostgres> {
    CompanyMongodb testMongoDb();

    CompanyES testElasticSearch();

    CompanyPostgres testPostgres();

    void testRedis();

    void publishToTopic(String message);
}
