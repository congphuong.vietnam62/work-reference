package com.npc.syncservice.domain.services;

import com.npc.syncservice.domain.entities.elasticsearch.CompanyES;
import com.npc.syncservice.domain.entities.mongodb.CompanyMongodb;
import com.npc.syncservice.domain.entities.postgres.CompanyPostgres;
import com.npc.syncservice.domain.services.serviceinterface.CompanyService;
import com.npc.syncservice.infrastructure.repositories.elasticsearch.CompanyESRepository;
import com.npc.syncservice.infrastructure.repositories.mongodb.CompanyMongoRepository;
import com.npc.syncservice.infrastructure.repositories.postgres.CompanyPostgresRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyServiceImp implements CompanyService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    private CompanyPostgresRepository companyPostgresRepository;

    @Autowired
    private CompanyMongoRepository companyMongoRepository;

    @Autowired
    private CompanyESRepository companyESRepository;

    @Override
    public CompanyPostgres getById(String id) {
        return companyPostgresRepository.getById(id);
    }

    @Override
    public CompanyMongodb testMongoDb() {
        CompanyMongodb company = new CompanyMongodb();

        company.setCode("a");
        company.setName("a");

        return companyMongoRepository.save(company);
    }

    @Override
    public CompanyES testElasticSearch() {
        CompanyES company = new CompanyES();


        company.setCode("a");
        company.setName("a");

        return companyESRepository.save(company);
    }


    @Override
    public CompanyPostgres testPostgres() {
        CompanyPostgres company = new CompanyPostgres();


        company.setCode("a");
        company.setName("a");

        return companyPostgresRepository.save(company);
    }


    @Override
    public void testRedis() {
        redisTemplate.opsForValue().set("Reference", "Hello World");

        System.out.println("value of key: " + redisTemplate.opsForValue().get("Reference"));
    }

    @Override
    public void publishToTopic(String message) {
        System.out.println("Message: " + message);

        this.kafkaTemplate.send("company-topic", message);
    }

    @Override
    public void deleteById(String id) {

    }

    @Override
    public void save(CompanyPostgres object) {
        this.companyPostgresRepository.save(object);
    }

    @Override
    public void update(CompanyPostgres object) {

    }

    @Override
    public List<CompanyPostgres> findAll() {
        return companyPostgresRepository.findAll();
    }
}
