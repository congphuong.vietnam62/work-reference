package com.npc.syncservice.domain.entities.mongodb;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document("companies")
public class CompanyMongodb {

    @Id
    private String id;

    @Indexed(unique = true)
    private String code;

    private String name;
}
