package com.npc.syncservice.infrastructure.events;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class CompanyConsumer {

    @KafkaListener(topics = "company-topic", groupId = "references")
    public void listenerFromCompany(String message) {
        System.out.println("Consumed message: " + message);
    }

}
