package com.npc.syncservice.infrastructure.repositories.elasticsearch;

import com.npc.syncservice.domain.entities.elasticsearch.CompanyES;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface CompanyESRepository extends ElasticsearchRepository<CompanyES, String> {

    List<CompanyES> findAll();
}
