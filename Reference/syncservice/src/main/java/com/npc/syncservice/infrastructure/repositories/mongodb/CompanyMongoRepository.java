package com.npc.syncservice.infrastructure.repositories.mongodb;

import com.npc.syncservice.domain.entities.mongodb.CompanyMongodb;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyMongoRepository extends MongoRepository<CompanyMongodb, String> {

    List<CompanyMongodb> findAll();
}
